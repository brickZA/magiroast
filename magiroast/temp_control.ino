#include <PID_v1.h>

enum class TempControlMode : uint8_t {manual, pid};

class TempControl
{
 public:
  double Kp=10, Ki=5, Kd=4;
  // See for explanation of P_ON_M mode:
  // http://brettbeauregard.com/blog/2017/06/introducing-proportional-on-measurement/
  const int POn = P_ON_M;
  // Use less than ADC max since we truncate when normalizing which means the
  // ADC reading must be at the very max value to register a quantized 100%.
  const float ANALOGUE_RANGE = 1018;
  const float temp_min = 150;
  const float temp_max = 240;
  const float temp_range = temp_max - temp_min;
  // Assuming output is heater power, input and set_point are in degC
  double input=0, output=0, set_point=0;
  void setTempControlMode(TempControlMode newMode) {
    mode = newMode;
    switch(mode) {
    case TempControlMode::manual: pid.SetMode(MANUAL); break;
    case TempControlMode::pid: pid.SetMode(AUTOMATIC); break;
    }
  }

  void setNextTempControlMode() {
    switch (mode) {
    case TempControlMode::manual: setTempControlMode(TempControlMode::pid); break;
    case TempControlMode::pid: setTempControlMode(TempControlMode::manual); break;
    default: mode = TempControlMode::manual; // OOPS?
    }
  }

  TempControlMode getTempControlMode() {
    return mode;
  }

  // Returns a 3 char string representation of operating mode
  char* mode_as_str3() {
    switch(mode) {
    case TempControlMode::manual: return "man";
    case TempControlMode::pid: return "pid";
    default: return "bad";
    }
  }

  void update(int potReading) {
    float normPotReading = min(potReading, ANALOGUE_RANGE) / ANALOGUE_RANGE;
    if (mode == TempControlMode::manual) {
      output = 100 * normPotReading;
    } else {
      set_point = temp_min + normPotReading * temp_range;
    }
    pid.Compute();
  }

  void setOutputLimits(double omin, double omax) {
    pid.SetOutputLimits(omin, omax);
  }

  void applyTunings() {
    pid.SetTunings(Kp, Ki, Kd, POn);
  }

 private:
  TempControlMode mode = TempControlMode::manual;
  PID pid = PID(&input, &output, &set_point, Kp, Ki, Kd, POn, DIRECT);
};
