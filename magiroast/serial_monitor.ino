class SerialMonitor {
 public:
  // Set whenever a variable is updated, user can set to false when handled.
  boolean change_event = false;

  SerialMonitor(double *Kp,
                double *Ki,
                double *Kd,
                double *temp,
                double *power,
                double *set_point) {
    // These indices should match the order of var_names
    var_ptrs[0] = Kp;
    var_ptrs[1] = Ki;
    var_ptrs[2] = Kd;
    var_ptrs[3] = temp;
    var_ptrs[4] = power;
    var_ptrs[5] = set_point;
  }

  // Read bytes from serial into buf if available up to EOL or max message size.
  // On EOL, interpret message.
  // If buffer is full, discard extra data.
  void interpret_input() {
    uint8_t bytes_read = 0;
    while (Serial.available()) {
      char in = Serial.read();
      bytes_read++;
      if (in == EOL) {
        buf[buf_i] = '\0';  // Ensure buf is terminated after last received char
        handle_message();
        buf_i = 0;          // Reset buffer index; ready for next message
      } else if (buf_i < max_msg_len) {
        buf[buf_i++] = in;
        buf[buf_i] = '\0';

      } if (bytes_read >= max_msg_len ) {
        break; // Stop reading if we've read max_msg_len so we don't hog event loop
      }
    }
  }

  // Cycle through all named vars and log to console. Logs one var per call to
  // avoid busting serial buffers. That does mean some time-skew between the
  // values. Should be compatible with arduino serial grapher:
  // https://diyrobocars.com/2020/05/04/arduino-serial-plotter-the-missing-manual/
  void monitor_process_vars() {
    Serial.print(var_names[mon_i]);
    Serial.print(":");
    Serial.print(*var_ptrs[mon_i]);
    mon_i++;
    mon_i = mon_i % num_mon_items;
    mon_i == 0 ? Serial.println() : Serial.print(",");
  }

 private:
  static const uint8_t max_msg_len = 16;
  static const char EOL = '\r';
  static const uint8_t num_mon_items = 6;
  static const uint8_t num_update_items = 3;
  char buf[max_msg_len+1];
  uint8_t buf_i = 0;
  double *var_ptrs[num_mon_items];
  const char *var_names[num_mon_items] =
    {"Kp", "Ki", "Kd", "temp", "power", "set_point"};
  uint8_t mon_i = 0;

  void handle_message() {
    char *tok = strtok(buf, "=");
    if (tok == NULL) {
      error("Missing ="); return;
    }
    char *val = strtok(NULL, "=");
    if (val == NULL) {
      error("Missing value"); return;
    }
    char *parse_end;
    // arduino (avr?) does not have strtof and on avr float = double, so whatever
    // https://arduino.stackexchange.com/questions/48714/undefined-reference-to-stdlib-strtof-while-compiling
    double parsed_val = strtod(val, &parse_end); 
    if (parse_end == val) {
      error("Invalid number"); return;
    }
    if (*parse_end != '\0') {
      error("Garbage"); return;
    }

    update_value(tok, parsed_val);
    change_event = true;
  }

  void update_value(char *variable_name, double value){
    boolean found = false;
    uint8_t var_i;
    for (uint8_t i=0; !found && i < num_update_items; i++) {
      var_i = i;
      found = strcmp(variable_name, var_names[i]) == 0;
    }

    if (!found) {
      error(variable_name);
      return;
    }

    *var_ptrs[var_i] = value;
  }

  void error(char *msg) {
    Serial.print("ERR: ");
    Serial.println(msg);
  }
};
