/* #include "thermistor.h" */

// Based on example https://learn.adafruit.com/thermistor/using-a-thermistor

// resistance at 25 degrees C
#define THERMISTORNOMINAL 100000
// temp. for nominal resistance (almost always 25 C)
#define TEMPERATURENOMINAL 25
// The beta coefficient of the thermistor (usually 3000-4000)
#define BCOEFFICIENT 3950
// the value of the 'other' resistor
#define SERIESRESISTOR 4700
// how many samples to take and average, more takes longer
// but is more 'smooth'. Check that NUM_THERMISTOR_SAMPLES * ADC_RANGE won't
// overflow ThermistorReading.adc_accumulator.
const uint8_t NUM_THERMISTOR_SAMPLES = 64;

const float KELVIN_OFFSET = 273.15;
const float TEMPERATURE_NOMINAL_KELVIN = TEMPERATURENOMINAL + KELVIN_OFFSET;
const uint16_t ADC_RANGE = 1023;

struct ThermistorReading {
  uint16_t adc_accumulator;
  float adc_average;
  float resistance;
  float temp_degrees_C;
};

// Assuming the fixed resistor is pull-up
struct ThermistorReading read_thermistor() {
  ThermistorReading reading;

  reading.adc_accumulator = 0;
  for (uint8_t i=0; i < NUM_THERMISTOR_SAMPLES; i++) {
    reading.adc_accumulator += analogRead(THERMISTOR_PIN);
  }

  reading.adc_average = reading.adc_accumulator;
  reading.adc_average /= NUM_THERMISTOR_SAMPLES;

  // convert the value to resistance
  reading.resistance = ADC_RANGE / reading.adc_average - 1;
  reading.resistance = SERIESRESISTOR / reading.resistance;

  float steinhart;
  steinhart = reading.resistance / THERMISTORNOMINAL; // (R/Ro)
  steinhart = log(steinhart);                         // ln(R/Ro)
  steinhart /= BCOEFFICIENT;                          // 1/B * ln(R/Ro)
  steinhart += 1.0 / (TEMPERATURE_NOMINAL_KELVIN);    // + (1/To)
  steinhart = 1.0 / steinhart;                        // Invert
  steinhart -= KELVIN_OFFSET;                         // convert to C

  reading.temp_degrees_C = steinhart;
  return reading;
}

