// This is the delay-per-brightness step in microseconds. For 60 Hz it should be 65
// const long freq_step = 75;      // Microseconds
const long freq_step = 38;      // Microseconds
const long slowdown = 1000 * 0 + 1;

const uint8_t HARD_ON = 8;      // Turn TRIAC 100% on for dim <= HARD_ON
const uint8_t HARD_OFF = 250;
volatile uint8_t dim = 134;      // Dimming level (0-255)  0 = on, 255 = 0ff
volatile uint16_t zeros_ctr = 0; // Number of zero-detection events
volatile uint8_t cnt_since_zero = 0; // Number of timer intervals since zero detection
// Boolean to store a "switch" to tell us if we have crossed zero
volatile boolean zero_cross = false;

const int LCD_LEN = 16;
// Screen seems to have address 0x3F Some screens apparently use address 0x27 or 0x38
LiquidCrystal_I2C  lcd(0x3F, 2, 1, 0, 4, 5, 6, 7, 3, POSITIVE);  // Set the LCD I2C address
char line0[LCD_LEN + 1];
char line1[LCD_LEN + 1];

TempControl temp_control = TempControl();
SerialMonitor serial_monitor = SerialMonitor(&temp_control.Kp,
                                             &temp_control.Ki,
                                             &temp_control.Kd,
                                             &temp_control.input,
                                             &temp_control.output,
                                             &temp_control.set_point);
// Look up 0-100% power -> dim count, calculated by re-scaling lineariation
// values from https://github.com/greencardigan/TC4-shield/blob/master/applications/Artisan/aArtisan_PID/trunk/src/aArtisanQ_PID/phase_ctrl.cpp
const uint8_t phase_linearization[] PROGMEM = {
    255, 225, 217, 211, 207, 203, 200, 196, 194, 191, 188, 186, 184,
    182, 180, 178, 176, 174, 172, 170, 169, 167, 165, 164, 162, 161,
    159, 158, 156, 155, 153, 152, 151, 149, 148, 146, 145, 144, 142,
    141, 140, 139, 137, 136, 135, 133, 132, 131, 130, 128, 127, 126,
    124, 123, 122, 121, 119, 118, 117, 115, 114, 113, 112, 110, 109,
    108, 106, 105, 103, 102, 101,  99,  98,  96,  95,  93,  92,  90,
    89,  87,  85,  84,  82,  80,  78,  76,  74,  72,  70,  68,  66,
    63,  60,  58,  54,  51,  47,  43,  37,  29,   0
};

void setup() {                                      // Begin setup
  digitalWrite(FAN_RELAY_PIN, 1);                   // Ensure relay is off at startup
  digitalWrite(AC_RELAY_PIN, 1);                    // Ensure relay is off at startup
  Serial.begin(BAUD);
  lcd_setup();
  triac_setup();
  pinMode(ROAST_PIN, INPUT_PULLUP);
  pinMode(MODE_TOGGLE_PIN, INPUT_PULLUP);
  pinMode(EXTRA_TOGGLE_PIN, INPUT_PULLUP);
  pinMode(FAN_RELAY_PIN, OUTPUT);
  pinMode(AC_RELAY_PIN, OUTPUT);
  analogReference(EXTERNAL);
  temp_control.setOutputLimits(0, 100);
}


void lcd_setup() {
  lcd.begin(LCD_LEN, 2);        // initialize the lcd
  lcd.home();                   // go home
  lcd.print("Looks like...");
  lcd.setCursor(0, 1);
  lcd.print("Magi-Roast v1.0!");
  delay(1000);
  clear_lcd_buffer();
}

void clear_lcd_buffer() {
  sprintf(line0, "%*s", LCD_LEN, "");
  sprintf(line1, "%*s", LCD_LEN, "");
}

void refresh_lcd() {
  lcd.setCursor(0,0);
  lcd.print(line0);
  lcd.setCursor(0, 1);
  lcd.print(line1);
}

// Converts float to string of max length 7 with 2 precision digits, otherwise prints "MAX" or "MIN"
void float_to_str(char * buff, float val) {
  if (val >= 1000.0) {
    sprintf(buff, "MAX");
  } else if (val <= -100) {
    sprintf(buff, "MIN");
  } else {
    dtostrf(val, 4, 2, buff);
  }
}

void triac_setup() {
  // Attach an Interupt to Pin 2 (interupt 0)  for Zero
  attachInterrupt(digitalPinToInterrupt(AC_ZERO_PIN), zero_cross_detect, RISING);
  pinMode(AC_pin, OUTPUT);	// Set the Triac pin as output
  digitalWrite(AC_pin, LOW);	// turn the TRIAC off by making the voltage LOW

  // Initialize TimerOne library with the dimming timestep
  Timer1.initialize(freq_step * slowdown);
  Timer1.attachInterrupt(triac_turn_on_check);
}

void zero_cross_detect() {
  // if zero_cross is already true probably a bounce
  if (!zero_cross) {
    zeros_ctr++;
    zero_cross = true;
    cnt_since_zero = 0;
  }
}
void triac_turn_on_check() {
  if (dim <= HARD_ON) {
    // Turn on hard when we are almost not dimming
    digitalWrite(AC_pin, HIGH); // turn TRIAC on
    return;
  }
  if (dim >= HARD_OFF)  {
    // Turn off hard when we are at max delay
    digitalWrite(AC_pin, LOW);
    return;
  }
  if (zero_cross ) {
    if (cnt_since_zero >= dim) {
      digitalWrite(AC_pin, HIGH); // turn TRIAC on
      zero_cross = false;   // We've handled this zero crossing
    } else {
      cnt_since_zero++;
    }
  } else {
    digitalWrite(AC_pin, LOW); // turn TRIAC off
  }
}


bool is_toggle_event(bool current_state, bool last_state, unsigned long current_time, unsigned long last_event_time) {
  if (current_state
      && current_state != last_state
      && (current_time - last_event_time) >= DEBOUNCE_MS
      ) {
    return true;
  }
  return false;
}

int cnt = 0;
bool led_on = false;
char temp_str[8];
char set_temp_str[8];
bool current_toggle_state = false;
bool last_toggle_state = false;
bool toggle_event = false;
unsigned long last_toggle_event_time = 0;
bool extra_toggle_event = false;
bool current_extra_toggle_state = false;
bool last_extra_toggle_state = false;
unsigned long last_extra_toggle_event_time = 0;
unsigned long cnt_time = 0;
float display_temp = 0;

void loop() {
  auto reading = read_thermistor();
  auto current_time = millis();
  temp_control.input = reading.temp_degrees_C;
  temp_control.update(analogRead(POT_PIN));
  dim = pgm_read_byte_near (&phase_linearization[(int) temp_control.output]);

  last_toggle_state = current_toggle_state;
  last_extra_toggle_state = current_extra_toggle_state;
  current_toggle_state = digitalRead(MODE_TOGGLE_PIN) == LOW;
  current_extra_toggle_state = digitalRead(EXTRA_TOGGLE_PIN) == LOW;

  if (is_toggle_event(current_toggle_state,
                   last_toggle_state,
                   current_time,
                   last_toggle_event_time)) {
    toggle_event = true;
    last_toggle_event_time = current_time;
  }
  if (is_toggle_event(current_extra_toggle_state,
                   last_extra_toggle_state,
                   current_time,
                   last_extra_toggle_event_time)) {
    extra_toggle_event = true;
    last_extra_toggle_event_time = current_time;
  }

  if (toggle_event) {
    toggle_event = false;
  }
  if (extra_toggle_event) {
    temp_control.setNextTempControlMode();
    extra_toggle_event = false;
  }
  if (cnt % 50 == 0) {
    display_temp = reading.temp_degrees_C;
  }


  if (cnt % 10 == 0) {
    // Update relay state. Should probably do proper debouncing, but only
    // updating every 100ms-ish should do the trick.
    int roast_reading = digitalRead(ROAST_PIN);
    digitalWrite(FAN_RELAY_PIN, roast_reading);
    digitalWrite(AC_RELAY_PIN, roast_reading);

    // Print stuff to the LCD.
    float_to_str(temp_str, display_temp);
    float_to_str(set_temp_str, temp_control.set_point);
    sprintf(line0, "T: %6sC P:%3d", temp_str, (int) temp_control.output);
    sprintf(line1, "S: %6sC M:%3s", set_temp_str, temp_control.mode_as_str3());
    refresh_lcd();

    // Update Serial Monitor
    serial_monitor.monitor_process_vars();
    serial_monitor.interpret_input();
    if (serial_monitor.change_event) {
      temp_control.applyTunings();
        serial_monitor.change_event = false;
    }
  }


  if (current_time - cnt_time >= 10) {
    cnt++; cnt_time = current_time;}
};
