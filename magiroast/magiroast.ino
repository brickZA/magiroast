#include <TimerOne.h> // Available from http://www.arduino.cc/playground/Code/Timer1
#include <Wire.h>
#include <LiquidCrystal_I2C.h>

const int D2 = 2;
const int D3 = 3;
const int D4 = 4;
const int D5 = 5;
const int D6 = 6;
const int D7 = 7;
const int D8 = 8;
const int D9 = 9;
const int D10 = 10;
const int D11 = 11;
const int D12 = 12;
const int D13 = 13;
const int BAUD = 9600;

const int LED = D13;

const int AC_pin = D11;         // Output to Opto Triac
const int AC_ZERO_PIN = D2;
const int AC_RELAY_PIN = D3;
const int FAN_RELAY_PIN = D4;
const int ROAST_PIN = D5;
const int MODE_TOGGLE_PIN = D6;
const int EXTRA_TOGGLE_PIN = D7;
const int ROAST_INDICATOR_PIN = D8;

const int THERMISTOR_PIN = A7; // which analog pin the thermistor is connected to
const int POT_PIN = A2;        // select the input pin for the potentiometer
// NOTE A4 and A5 are used by Wire lib for I2C. Don't have much choice about
// that unless we want to use a software I2C library

const unsigned long DEBOUNCE_MS = 100;

